package com.example.medicmado5.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.medicmado5.ui.theme.descriptionColor
import com.example.medicmado5.ui.theme.latoFont
import com.example.medicmado5.ui.theme.sourceSansPro
import com.example.medicmado5.ui.theme.titleColor

/*
Описание: Компонент приветсвенного экрана
Дата создания: 29.03.2023
Автор: Георгий Хасанов
 */
@Composable
fun OnboardComponent(
    title: String,
    text: String
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .widthIn(max = 220.dp)
            .fillMaxWidth()
    ) {
        Text(
            title,
            fontSize = 20.sp,
            fontWeight = FontWeight.Bold,
            fontFamily = latoFont,
            textAlign = TextAlign.Center,
            color = titleColor
        )
        Spacer(modifier = Modifier.height(29.dp))
        Text(
            text,
            fontSize = 14.sp,
            fontWeight = FontWeight.Normal,
            fontFamily = sourceSansPro,
            textAlign = TextAlign.Center,
            color = descriptionColor
        )
    }
}