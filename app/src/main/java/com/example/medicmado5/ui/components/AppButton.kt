package com.example.medicmado5.ui.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.medicmado5.ui.theme.*
import com.example.medicmado5.R
/*
Описание: Стандартная кнопка приложения
Дата создания: 29.03.2023
Автор: Георгий Хасанов
 */
@Composable
fun AppButton(
    modifier: Modifier = Modifier,
    text: String,
    fontSize: TextUnit = 17.sp,
    fontWeight: FontWeight = FontWeight.SemiBold,
    fontFamily: FontFamily = sourceSansPro,
    color: Color = Color.White,
    enabled: Boolean = true,
    contentPadding: PaddingValues = PaddingValues(16.dp),
    colors: ButtonColors = ButtonDefaults.buttonColors(backgroundColor = primaryColor, disabledBackgroundColor = primaryDisabledColor),
    borderStroke: BorderStroke = BorderStroke(0.dp, strokeColor),
    onClick: () -> Unit
) {
    Button(
        modifier = modifier,
        shape = MaterialTheme.shapes.medium,
        contentPadding = contentPadding,
        elevation = ButtonDefaults.elevation(0.dp),
        border = borderStroke,
        enabled = enabled,
        colors = colors,
        onClick = onClick
    ) {
        Text(
            text = text,
            fontSize = fontSize,
            fontWeight = fontWeight,
            fontFamily = fontFamily,
            color = color
        )
    }
}

/*
Описание: Кнопка перехода на предыдущий экран
Дата создания: 29.03.2023
Автор: Георгий Хасанов
 */
@Composable
fun AppBackButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit
) {
    Card(
        shape = MaterialTheme.shapes.small,
        backgroundColor = inputColor,
        elevation = 0.dp,
        modifier = modifier.clickable {
            onClick()
        }
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_back),
            contentDescription = "",
            tint = textColor,
            modifier = Modifier.padding(6.dp)
        )
    }
}
